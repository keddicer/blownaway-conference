$(document).ready(function(){

    /* ------------------------------------------- user menu */

    $('#user').hover(function(){
        $('.submenu').css('height', '95');
    },function(){
        $('.submenu').css('height', '0');
    });
    $('.submenu').hover(function(){
        $(this).css('height', '95');
    },function(){
        $(this).css('height', '0');
    });

    /* ------------------------------------------- popups */

    $('#changePassCta').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#changePass>div').height();

        if(popup_realheight>popup_height) {
            $('#changePass').css('height', popup_realheight+190);
        } else {
            $('#changePass').css('height', popup_height);
        }
        // extra
        $('#changePass').css('z-index', '1000');
        $('#changePass>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#jimStroud').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#jimStroudBio>div').height();

        if(popup_realheight>popup_height) {
            $('#jimStroudBio').css('height', popup_realheight+190);
        } else {
            $('#jimStroudBio').css('height', popup_height);
        }
        // extra
        $('#jimStroudBio').css('z-index', '1000');
        $('#jimStroudBio>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#willLastname').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#willLastnameBio>div').height();

        if(popup_realheight>popup_height) {
            $('#willLastnameBio').css('height', popup_realheight+190);
        } else {
            $('#willLastnameBio').css('height', popup_height);
        }
        // extra
        $('#willLastnameBio').css('z-index', '1000');
        $('#willLastnameBio>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#smootCarter').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#smootCarterBio>div').height();

        if(popup_realheight>popup_height) {
            $('#smootCarterBio').css('height', popup_realheight+190);
        } else {
            $('#smootCarterBio').css('height', popup_height);
        }
        // extra
        $('#smootCarterBio').css('z-index', '1000');
        $('#smootCarterBio>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#selectService').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#servicesPopup>div').height();

        if(popup_realheight>popup_height) {
            $('#servicesPopup').css('height', popup_realheight+190);
        } else {
            $('#servicesPopup').css('height', popup_height);
        }
        // extra
        $('#servicesPopup').css('z-index', '1000');
        $('#servicesPopup>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('.upcoming').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#schedule>div').height();

        if(popup_realheight>popup_height) {
            $('#schedule').css('height', popup_realheight+190);
        } else {
            $('#schedule').css('height', popup_height);
        }
        // extra
        $('#schedule').css('z-index', '1000');
        $('#schedule>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#selectProvider').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#providersPopup>div').height();
        var popup_realheight2 = $('.providerReview>div').height();

        if(popup_realheight>popup_height) {
            $('#providersPopup').css('height', popup_realheight+190);
        } else {
            $('#providersPopup').css('height', popup_height);
        }
        if(popup_realheight2>popup_height) {
            $('.providerReview').css('height', popup_realheight+190);
        } else {
            $('.providerReview').css('height', popup_height);
        }
        // extra
        $('#providersPopup').css('z-index', '1000');
        $('.providerReview').css('z-index', '1000');
        $('#providersPopup>div').css('top', '30px');
        $('.providerReview>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#moreProvider1').click(function(){
        $('.review1').css('z-index', '1002');
        $('.review1').css('opacity', '1');
    });

    $('#moreProvider2').click(function(){
        $('.review2').css('z-index', '1002');
        $('.review2').css('opacity', '1');
    });

    $('.goBack').click(function(){
        $('.review1').css('opacity', '0');
        $('.review2').css('opacity', '0');
        setTimeout(function() { $('.review1').css('z-index', '1001'); }, 500);
        setTimeout(function() { $('.review2').css('z-index', '1001'); }, 500);
    });

    $('#forgot-pass-cta').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#forgot-pass-popup>div').height();

        if(popup_realheight>popup_height) {
            $('#forgot-pass-popup').css('height', popup_realheight+190);
        } else {
            $('#forgot-pass-popup').css('height', popup_height);
        }
        // extra
        $('#forgot-pass-popup').css('z-index', '100001');
        $('#forgot-pass-popup>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('.popOutFP').click(function(){
        $('#forgot-pass-popup>div').css('top', '-2000px');
        $('#forgot-pass-popup>.focus').css('opacity', '0');
        setTimeout(function() { $('#forgot-pass-popup').css('z-index', '-2'); }, 500);
        setTimeout(function() { $('#forgot-pass-popup').css('height', '0'); }, 500);
    });

    $('#sign-up-cta').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#sign-up-popup>div').height();

        if(popup_realheight>popup_height) {
            $('#sign-up-popup').css('height', popup_realheight+190);
        } else {
            $('#sign-up-popup').css('height', popup_height);
        }
        // extra
        $('#sign-up-popup').css('z-index', '100000');
        $('#sign-up-popup>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#sign-in-cta').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#sign-in-popup>div').height();

        if(popup_realheight>popup_height) {
            $('#sign-in-popup').css('height', popup_realheight+190);
        } else {
            $('#sign-in-popup').css('height', popup_height);
        }
        // extra
        $('#sign-in-popup').css('z-index', '100000');
        $('#sign-in-popup>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#selectAddon').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#addonsPopup>div').height();

        if(popup_realheight>popup_height) {
            $('#addonsPopup').css('height', popup_realheight+190);
        } else {
            $('#addonsPopup').css('height', popup_height);
        }
        // extra
        $('#addonsPopup').css('z-index', '100001');
        $('#addonsPopup>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('#selectPromocode').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#promocodePopup>div').height();

        if(popup_realheight>popup_height) {
            $('#promocodePopup').css('height', popup_realheight+190);
        } else {
            $('#promocodePopup').css('height', popup_height);
        }
        // extra
        $('#promocodePopup').css('z-index', '100001');
        $('#promocodePopup>div').css('top', '30px');
        $('.focus').css('opacity', '1');
    });

    $('.popOutAO').click(function(){
        $('#addonsPopup>div').css('top', '-2000px');
        $('#addonsPopup>.focus').css('opacity', '0');
        setTimeout(function() { $('#addonsPopup').css('z-index', '-2'); }, 500);
        setTimeout(function() { $('#addonsPopup').css('height', '0'); }, 500);
    });

    $('.popOutPC').click(function(){
        $('#promocodePopup>div').css('top', '-2000px');
        $('#promocodePopup>.focus').css('opacity', '0');
        setTimeout(function() { $('#promocodePopup').css('z-index', '-2'); }, 500);
        setTimeout(function() { $('#promocodePopup').css('height', '0'); }, 500);
    });

    $('.rateServiceCta').click(function(){
        goUp();
        // height
        var popup_height = $('body').height();
        var popup_realheight = $('#rateServicePopup>div').height();

        if(popup_realheight>popup_height) {
            $('#rateServicePopup').css('height', popup_realheight+190);
        } else {
            $('#rateServicePopup').css('height', popup_height);
        }
        // extra
        $('#rateServicePopup').css('z-index', '1000');
        $('#rateServicePopup>div').css('top', '30px');
        $('#rateServicePopup>.focus').css('opacity', '1');
    });

    $('#submitFeedback').click(function(){
        goUp();
        $('#rateServicePopup>div').css('top', '-2000px');
        $('#rateServicePopup>.focus').css('opacity', '0');
        setTimeout(function() { $('#rateServicePopup').css('z-index', '-2'); }, 500);
        setTimeout(function() { $('#thanksPopup').css('z-index', '1000'); }, 600);
        setTimeout(function() { $('#thanksPopup>.focus').css('opacity', '1'); }, 600);
        setTimeout(function() { $('#thanksPopup>div').css('top', '30px'); }, 600);
    });

    function goUp() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }

    $('.popOut').click(function(){
        $('.popup>div').css('top', '-2000px');
        $('.focus').css('opacity', '0');
        $('.providerReview').css('opacity', '0');
        setTimeout(function() { $('.popup').css('z-index', '-2'); }, 500);
        setTimeout(function() { $('.popup').css('height', '0'); }, 500);
    });

    /* ------------------------------------------- responsive menu */

    function responsiveMenuDisplayOn() {
        $('.rightMenu').css('z-index', '500');
        $('.rightMenu').css('right', '0');
        $('.header').css('top', '-55px');
        $('.focus').css('opacity', '.8');
        $('#contentRightMenu').css('right', '0');
        $('body').css('overflow-y', 'hidden');
    };
    function responsiveMenuDisplayOff() {
        $('.rightMenu').css('right', '-225px');
        $('.header').css('top', '0');
        $('#contentRightMenu').css('right', '-225px');
        $('.focus').css('opacity', '0');
        $('body').css('overflow-y', 'inherit');
        setTimeout(function() { $('.rightMenu').css('z-index', '-1'); }, 500);
    };
    $('#rightMenuIcon').click(function(){ responsiveMenuDisplayOn(); });
    $('.focus').click(function(){ responsiveMenuDisplayOff(); });

    /* ------------------------------------------- obtener alto */

    function createAccountHeight() {

        var realAccountHeight = $('#createAccount').height();
        var accountHeightWithPadding = realAccountHeight+155;
        var bodyHeight = $('body').height();

        if (accountHeightWithPadding > bodyHeight) {
            $('body').css('height', accountHeightWithPadding);
        }
    };
    function aboutYouHeight() {

        var realAccountHeight = $('#aboutYou').height();
        var accountHeightWithPadding = realAccountHeight+155;
        var bodyHeight = $('body').height();

        if (accountHeightWithPadding > bodyHeight) {
            $('body').css('height', accountHeightWithPadding);
        }
    };
    function membershipsHeight() {

        var realAccountHeight = $('#memberships').height();
        var accountHeightWithPadding = realAccountHeight+155;
        var bodyHeight = $('body').height();

        if (accountHeightWithPadding > bodyHeight) {
            $('body').css('height', accountHeightWithPadding);
        }
    };
    function fpassHeight() {

        var fpassHeight = $('#signIn').height();
        var fpassHeightWithPadding = fpassHeight+205;
        var bodyHeight = $('body').height();

        if (fpassHeightWithPadding > bodyHeight) {
            $('body').css('height', fpassHeightWithPadding);
        }
    };

    aboutYouHeight();
    createAccountHeight();
    membershipsHeight();
    fpassHeight();

    $(window).resize(function() {
        aboutYouHeight();
        createAccountHeight();
        membershipsHeight();
        fpassHeight();
    });

    /* ------------------------------------------- wizard laps */

    $('#accountCta').click(function(){
        $('#createAccount').css('opacity', '0');
        $('#createAccount').css('z-index', '-1');
        $('#aboutYou').css('left', '0');
        $('#aboutYou .box').css('display', 'block');
        $('#aboutYou').css('z-index', '0');
        aboutYouHeight();
    });
    $('#aboutCta').click(function(){
        $('#aboutYou').css('opacity', '0');
        $('#aboutYou').css('z-index', '-1');
        $('#memberships').css('left', '0');
        $('#memberships .box').css('display', 'block');
        $('#memberships').css('z-index', '0');
        setTimeout(function() { membershipsHeight(); }, 500);
    });

});
